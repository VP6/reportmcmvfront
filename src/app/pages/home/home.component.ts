import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { ScriptLoaderService } from '../../_services/script-loader.service';
declare var $ : any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  empreendimento: any[] = [
    {
      emp1: 'Empreendimento 1',
      emp2: 'Empreendimento 2',
      emp3: 'Empreendimento 3'
    }
  ]

  columns: any[] = ["indicador", "total", "jan/2018", "fev/2018"];

  dataInicial: string = "01/01/2018";
  dataFinal: string = "07/01/2018";

  data: any[] = [
    {  
      "data": {
        "indicador": 'VENDAS NO PERÍODO (UH)', 
        "total": "500.000"
      },
      "children" : [
        {data: { "indicador" : "MÓDULO I", "total": "90" },},
        {data: {"indicador" : "MÓDULO II", "total": "500.000"},},
        {data: {"indicador" : "MÓDULO III", "total": "135.000"},},
        {data: {"indicador" : "MÓDULO IV", "total": "90"},}
      ],
    },
    {  
      "data": {
        "indicador": 'VENDAS NO PERÍODO (R$)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'TICKET MÉDIO ACUMULADO (R$)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'PERCENTUAL DE VENDAS (%)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VENDAS ACUMULADAS (UH)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VENDAS ACUMULADAS (R$)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'DISTRATOS NO PERÍODO (UH)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'DISTRATOS NO PERÍODO (R$)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'DISTRATOS ACUMULADOS (UH)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'DISTRATOS ACUMULADO (R$)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'DISTRATOS s/ VENDAS (%)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VENDAS LÍQUIDAS (UH)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VENDAS LÍQUIDAS (R$)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'ESTOQUE (UH)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'ESTOQUE (R$)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'CONTRATAÇÕES NO PERÍODO (UH)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'CONTRATAÇÕES ACUMULADAS (UH)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VELOCIDADE CONTRATAÇÃO (DC)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'EM TRATAMENTO PARA ASSINATURA (UH)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'REGISTROS NO PERÍODO (UH)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'REGISTROS ACUMULADOS (UH)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'VELOCIDADE REGISTRO (DC)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'EM TRATAMENTO PARA REGISTRO (UH)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'RECEBIDO CARTEIRA PRÓPRIA (R$)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },
         "children":[  
          { data: { "indicador": "SINAL" }},
          { data: { "indicador": "MENSAL"}},
          { data: { "indicador": "INTERMEDIÁRIA"}}                
      ]
        }
      ],
    },
    {  
      "data": {
        "indicador": 'CONTAS A RECEBER CARTEIRA PRÓPRIA (R$)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },
         "children":[  
          { data: { "indicador": "SINAL" }},
          { data: { "indicador": "MENSAL"}},
          { data: { "indicador": "INTERMEDIÁRIA"}}                
      ]
        }
      ],
    },
    {  
      "data": {
        "indicador": 'BLOQUEIOS (R$)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'CURVA OBRA RAE (%)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'CURVA OBRA RAE ACUMULADA (%)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'LIMITE DISPONÍVEL PJ (R$)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },}
      ],
    },
    {  
      "data": {
        "indicador": 'SALDO DEVEDOR PJ (R$)', 
        "total": "500.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "500.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'FINANCIAMENTO PF Recebido (R$)', 
        "total": "135.000"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "135.000" },}
      ],
    },
    {  
      "data": {
        "indicador": 'FINANCIAMENTO PJ Recebido (R$)', 
        "total": "90"
      },
      "children" : [
         { data: { "indicador" : "MÓDULO I", "total": "90" },
        }
      ],
    },
  ]

  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {
    $("#fim").change(function() {
      this.dataFinal = $(this).val();
      this.dataInicial = $('#inicio').val();
      let cols = [];

      let mesInicial, anoInicial, mesFinal, anoFinal;

      mesInicial = this.dataInicial.split("/")[0];
      mesFinal = this.dataFinal.split("/")[0];

      anoInicial = this.dataInicial.split("/")[2];
      anoFinal = this.dataFinal.split("/")[2];

      let error = false;
      this.columns = [];
      if (anoFinal == anoInicial) {
        if (mesFinal > mesInicial) {
          for (let index = mesInicial; index <= mesFinal; index++) {
            let col = index % 12 == 1 ? "jan" :
                      index % 12 == 2 ? "fev" : 
                      index % 12 == 3 ? "mar" : 
                      index % 12 == 4 ? "abr" : 
                      index % 12 == 5 ? "mai" : 
                      index % 12 == 6 ? "jun" : 
                      index % 12 == 7 ? "jul" : 
                      index % 12 == 8 ? "ago" : 
                      index % 12 == 9 ? "set" : 
                      index % 12 == 10 ? "out" : 
                      index % 12 == 11 ? "nov" : 
                      index % 12 == 0 ? "dez" : "";
            col = col + "/" + anoInicial;
            this.columns.push(col);
          }
        } else error = true;
      } else error = true;

      if (error) alert("Range inválido");

      //this.setColumns(this.columns);
    })
  }

  setColumns(cols) {
    this.columns = cols;
  }

  getNameOfMonth(index: number): string {
    return index % 12 == 0 ? "jan" :
          index % 12 == 1 ? "fev" : 
          index % 12 == 2 ? "mar" : 
          index % 12 == 3 ? "abr" : 
          index % 12 == 4 ? "mai" : 
          index % 12 == 5 ? "jun" : 
          index % 12 == 6 ? "jul" : 
          index % 12 == 7 ? "ago" : 
          index % 12 == 8 ? "set" : 
          index % 12 == 9 ? "out" : 
          index % 12 == 10 ? "nov" : 
          index % 12 == 11 ? "dez" : ""; 
  }

  ngAfterViewInit() {
    this._script.load('./assets/js/scripts/form-plugins.js');
  }
  /*ngAfterViewInit() {
    this._script.load('./assets/js/scripts/dashboard_visitors.js');
  }*/

}